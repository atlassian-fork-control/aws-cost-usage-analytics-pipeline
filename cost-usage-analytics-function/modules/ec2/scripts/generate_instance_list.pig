COST_REPORT = LOAD '$INPUT' USING org.apache.pig.piggybank.storage.CSVExcelStorage(',', 'YES_MULTILINE', 'NOCHANGE', 'SKIP_INPUT_HEADER') as ($COST_REPORT_INPUT_SCHEMA);

EC2_USAGE_COSTS = FILTER COST_REPORT BY (lineItem::UsageType matches '.*Usage:.*') AND (lineItem::ProductCode == 'AmazonEC2') AND (lineItem::LineItemType == 'Usage') AND (lineItem::AvailabilityZone != '');

INSTANCE_HOURS = FOREACH EC2_USAGE_COSTS GENERATE lineItem::UsageAccountId,lineItem::AvailabilityZone,lineItem::ResourceId,(chararray)lineItem::UsageStartDate,(chararray)lineItem::UsageEndDate;

INSTANCE_HOURS_GROUPED = GROUP INSTANCE_HOURS by (lineItem::UsageAccountId,lineItem::AvailabilityZone,lineItem::ResourceId);




INSTANCE_LIST = FOREACH INSTANCE_HOURS_GROUPED GENERATE
                   flatten(group.lineItem::UsageAccountId) AS UsageAccountId,
                   flatten(group.lineItem::AvailabilityZone) AS AvailabilityZone,
                   flatten(group.lineItem::ResourceId) AS ResourceId,
                   MIN(INSTANCE_HOURS.lineItem::UsageStartDate) AS MinUsageStartDate,
                   MAX(INSTANCE_HOURS.lineItem::UsageEndDate) AS MaxUsageEndDate;

store INSTANCE_LIST into '$OUTPUT' USING org.apache.pig.piggybank.storage.CSVExcelStorage(',', 'NO_MULTILINE', 'UNIX', 'SKIP_OUTPUT_HEADER');
