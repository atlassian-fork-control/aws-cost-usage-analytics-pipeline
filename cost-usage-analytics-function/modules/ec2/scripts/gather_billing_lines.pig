COST_REPORT = LOAD '$INPUT' USING org.apache.pig.piggybank.storage.CSVExcelStorage(',', 'NO_MULTILINE', 'NOCHANGE', 'SKIP_INPUT_HEADER') as ($COST_REPORT_INPUT_SCHEMA);
EC2_USAGE_COSTS = FILTER COST_REPORT BY (lineItem::UsageType matches '.*Usage:.*') AND (lineItem::ProductCode == 'AmazonEC2') AND (lineItem::LineItemType == 'Usage') AND DaysBetween(CurrentTime(), ToDate((chararray)lineItem::UsageStartDate))<$RETENTION+1;
store EC2_USAGE_COSTS into '$OUTPUT' USING org.apache.pig.piggybank.storage.CSVExcelStorage(',', 'NO_MULTILINE', 'UNIX', 'SKIP_OUTPUT_HEADER');
