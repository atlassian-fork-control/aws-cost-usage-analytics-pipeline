#!/usr/bin/python2.7
from __future__ import print_function
from boto3.session import Session as Boto3Session
from botocore.session import Session as BotocoreSession
from botocore.credentials import RefreshableCredentials, create_assume_role_refresher
import sys
import os
import logging
from time import sleep
from datetime import datetime, timedelta


logging.basicConfig(level=logging.DEBUG)
logging.getLogger("boto3").setLevel(logging.WARNING)
logging.getLogger("botocore").setLevel(logging.WARNING)
logging.getLogger("botocore.auth").setLevel(logging.WARNING)
logging.getLogger("botocore.hooks").setLevel(logging.WARNING)
logging.getLogger("botocore.endpoint").setLevel(logging.WARNING)
logging.getLogger("botocore.client").setLevel(logging.WARNING)
logging.getLogger("botocore.parsers").setLevel(logging.WARNING)


def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)


class CrossAccountBotoCoreSession(BotocoreSession):
    def __init__(self, sts_role_config, default_session):
        self.sts_role_config = sts_role_config
        self.default_session = default_session
        super(CrossAccountBotoCoreSession, self).__init__()

    def get_credentials(self):
        if self._credentials is None:
            self.set_credentials()
        return self._credentials

    def set_credentials(self, access_key=None, secret_key=None, token=None):
        client = self.default_session.client('sts')
        refresher = create_assume_role_refresher(client, params=self.sts_role_config)
        self._credentials = RefreshableCredentials.create_from_metadata(metadata=refresher(),
                                                                        refresh_using=refresher,
                                                                        method='assume-role')


class Session(Boto3Session):
    def __init__(self, sts_role_config=None, **kwargs):
        if sts_role_config:
            default_session = Boto3Session(**kwargs)
            session = CrossAccountBotoCoreSession(sts_role_config=sts_role_config,
                                                  default_session=default_session)
            region = kwargs.get('region_name', 'us-east-1')
            super(Session, self).__init__(botocore_session=session, region_name=region)
        else:
            super(Session, self).__init__(**kwargs)


class ConnectionManager:
    def __init__(self, **kwargs):
        self.default_session_args = kwargs
        self.connections = {}
        self.logger = logging.getLogger(__name__)

    def get_session(self, role_arn, role_session_name):
        account_id = role_arn.split(':')[4]
        if account_id not in self.connections:
            self.connections[account_id] = Session(sts_role_config={"RoleArn": role_arn,
                                                                    "RoleSessionName": role_session_name},
                                                   **self.default_session_args)
        return self.connections[account_id]


class StreamProcessor:
    def __init__(self):
        self.connections = ConnectionManager()
        self.role_name = os.environ.get('ROLE', 'aws_cost_usage_pipeline_role')
        self.retention = int(os.environ.get('RETENTION', 14))
        self.retention_date = datetime.now() - timedelta(days=self.retention)
        self.retention_date = self.retention_date.replace(hour=0, minute=0, second=0, microsecond=0)
        self.retention_date_string = self.retention_date.strftime('%Y-%m-%dT%H:%M:%SZ')
        self.role_session_name = self.role_name

    def process_stream(self):
        for line in sys.stdin:
            line = line.strip()
            if line:
                account_id, az, instance_id, start_datetime, end_datetime = line.split(",")
                if end_datetime < self.retention_date_string:  # No data in cloudwatch for this line.
                    eprint('skipping line for old ec2 instance')
                    continue  # Skip record
                region = az[:-1]
                role_arn = 'arn:aws:iam::{}:role/{}'.format(account_id, self.role_name)
                try:
                    session = self.connections.get_session(role_arn=role_arn, role_session_name=self.role_name)

                    m = Metrics(region=region,
                                account_id=account_id,
                                start_time=start_datetime,
                                end_time=end_datetime,
                                session=session,
                                period=3600)
                    m.get_ec2_metrics(instance_id)
                except Exception as e:
                    eprint(e)


class Metrics:
    def __init__(self,
                 region,
                 account_id,
                 start_time,
                 end_time,
                 session,
                 file_handle=sys.stdout,
                 period=60):
        self.region = region
        self.account_id = account_id
        self.file_handle = file_handle
        self.period = period
        self.session = session
        self.client = self.session.client('cloudwatch', self.region)
        self.metric_unit_map = {'CPUUtilization': 'Percent',
                                'NetworkIn': 'Bytes',
                                'NetworkOut': 'Bytes',
                                'DiskReadBytes': 'Bytes',
                                'DiskWriteBytes': 'Bytes',
                                'DiskReadOps': 'Count',
                                'DiskWriteOps': 'Count'
                                }

        self.metric_stats_map = {'CPUUtilization': ['Maximum', 'Minimum', 'Average'],
                                 'NetworkIn': ['Maximum', 'Minimum', 'Average'],
                                 'NetworkOut': ['Maximum', 'Minimum', 'Average'],
                                 'DiskReadBytes': ['Maximum', 'Minimum', 'Average'],
                                 'DiskWriteBytes': ['Maximum', 'Minimum', 'Average'],
                                 'DiskReadOps': ['Maximum', 'Minimum', 'Average'],
                                 'DiskWriteOps': ['Maximum', 'Minimum', 'Average']
                                 }
        self.end = end_time
        self.start = start_time
        self.metric_list = sorted(self.metric_stats_map.keys())

    def safe_call_get_metric_statistics(self, kwargs):
        back_off = 1
        retries = 20
        metrics = None
        while retries:
            try:
                metrics = self.client.get_metric_statistics(**kwargs)
            except Exception as e:
                if e.message and 'Rate exceeded' in e.message:
                    eprint('API Rate Limit Exceeded backing off')
                    sleep(5 * back_off)
                    back_off += 1
                    retries -= 1
                else:
                    eprint(e)
                    break
            else:
                break
        return metrics

    def get_ec2_metrics(self,  instance_id):
        eprint('Gathering metrics on Account {0} for instance {1} between {2} and {3}'.format(self.account_id,
                                                                                              instance_id,
                                                                                              self.start,
                                                                                              self.end))
        instance_data = dict()
        for metric_name in self.metric_list:
            statistics = self.metric_stats_map[metric_name]
            try:
                kwargs = {'Namespace': 'AWS/EC2',
                          'MetricName': metric_name,
                          'Dimensions': [{'Name': 'InstanceId', 'Value': instance_id}],  # noqa
                          'StartTime': self.start,
                          'EndTime': self.end,
                          'Period': self.period,
                          'Statistics': statistics,
                          'Unit': self.metric_unit_map[metric_name]
                          }
                metric_data_points = self.safe_call_get_metric_statistics(kwargs=kwargs)
            except Exception as e:
                eprint(e)
                continue
            if 'Datapoints' in metric_data_points:
                for data_point in metric_data_points['Datapoints']:
                    date_stamp = data_point['Timestamp'].strftime("%Y-%m-%dT%H:%M:%SZ") #eg: 2015-12-26T02:00:00Z
                    if date_stamp not in instance_data:
                        instance_data[date_stamp] = dict()
                    instance_data[date_stamp][metric_name] = ','.join([str(data_point['Minimum']),
                                                                       str(data_point['Average']),
                                                                       str(data_point['Maximum'])])

        for date_stamp, metrics in instance_data.iteritems():
            output = ','.join([self.account_id, instance_id, date_stamp])
            for metric in self.metric_list:
                output += ','
                if metric in metrics:
                    output += metrics[metric]
                else:
                    output += ',,'
            print(output)


if __name__ == "__main__":
    StreamProcessor().process_stream()



