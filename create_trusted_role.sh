#!/bin/bash

ROLE_NAME=$(cat ./_meta/variables/s-variables-* | grep '"crossAccountRoleName"' | awk -F'"' '{ print $4 }')
TRUST_ROLE_NAME=$(cat ./_meta/variables/s-variables-* | grep '"dataPipelineEMREC2Role"' | awk -F'"' '{ print $4 }')
echo "{
  \"Version\": \"2012-10-17\",
  \"Statement\": [
    {
      \"Sid\": \"\",
      \"Effect\": \"Allow\",
      \"Principal\": {
        \"AWS\": [
          \"${TRUST_ROLE_NAME}\"
        ]
      },
      \"Action\": \"sts:AssumeRole\"
    }
  ]
}" > trust_policy.template
aws iam create-role --role-name ${ROLE_NAME} --assume-role-policy-document file://trust_policy.template
aws iam update-assume-role-policy --role-name ${ROLE_NAME} --policy-document file://trust_policy.template
aws iam attach-role-policy --role-name ${ROLE_NAME} --policy-arn "arn:aws:iam::aws:policy/CloudWatchReadOnlyAccess"
